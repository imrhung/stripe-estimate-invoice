<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estimate extends CI_Controller {
	public function addItem()
	{		
		$this->load->model('Item_model');
		$rs = $this->Item_model->addItemEstimate();
		echo json_encode($rs);
	}
	public function removeItem()
	{
		$this->load->model('Item_model');
		$rs = $this->Item_model->removeItem();
		echo json_encode($rs);
	}
	public function getItem()
	{
		$this->load->model('Item_model');
		$rs = $this->Item_model->getItem();
		echo json_encode($rs);
	}
	public function recentOpenEstimates()
	{
		$this->load->model('Estimate_model');
		$data['query'] = $this->Estimate_model->getRecentOpenEstimates();
		echo json_encode($data['query']);
	}
	public function recentClosedEstimates()
	{
		$this->load->model('Estimate_model');
		$data['query'] = $this->Estimate_model->getRecentCloseEstimates();
		echo json_encode($data['query']);
	}
	public function createEstimate()
	{
		$this->load->model('Client_model');
		$rs = $this->Client_model->createClientEstimate();
		echo json_encode($rs);
	}
	public function getEstimateInfo()
	{
		$this->load->model('Estimate_model');
		$data['query'] = $this->Estimate_model->getEstimateData();
		echo json_encode($data['query']);
	}
	public function getEstimateItemList()
	{
		$this->load->model('Estimate_model');
		$data['query'] = $this->Estimate_model->getItems();
		echo json_encode($data['query']);
	}
	public function updateEstimateInfo()
	{
		$this->load->model('Estimate_model');
		$data['query'] = $this->Estimate_model->updateEstimateData();
		echo json_encode($data['query']);
	}
}

/* End of file estimate.php */
/* Location: ./application/controllers/estimate.php */