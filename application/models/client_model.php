<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_model extends CI_Model {

    function Client_model() {
        parent::__construct();
    }

    function createClientEstimate() {
        $this->load->database();

        $parameters = array($this->input->post('clientName'),
            $this->input->post('clientEmail'),
            $this->input->post('clientAddress'),
            $this->input->post('clientSummary'),
            $this->input->post('createdDate')
        );

        $sql = "CALL add_estimate(?,?,?,?,?)";
        $query = $this->db->query($sql, $parameters);
        
        // Stick the estimate with the uid.
        
        
        return $query->result();
    }

}
