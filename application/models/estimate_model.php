<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Estimate_model extends CI_Model {

    function Estimate_model() {
        parent::__construct();
    }

    function getRecentOpenEstimates() {
        $this->load->database();
        $sql = "CALL get_recent_open_estimate";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        return $query->result();
    }

    function getRecentCloseEstimates() {
        $this->load->database();
        $sql = "CALL get_recent_closed_estimate";
        $parameters = array();
        $query = $this->db->query($sql, $parameters);
        return $query->result();
    }

    function getEstimateData() {
        $this->load->database();
        $parameters = array($this->input->post('estimate_id'));
        $query = $this->db->query('CALL get_estimate(?)', $parameters);
        return $query->result();
    }

    function getItems() {
        $this->load->database();
        $query = $this->db->query('select * from item where estimate_id = ' . $this->input->post('estimate_id'));
        return $query->result();
    }

    function updateEstimateData() {
        $this->load->database();
        /* $this->db->trans_start(); */
        $parameters = array(
            $this->input->post('estimateId'),
            $this->input->post('clientAddress'),
            $this->input->post('clientSummary')
        );
        $query = $this->db->query('CALL update_estimate(?,?,?)', $parameters);

        /* $this->db->trans_complete(); */
        return $query->result();
    }

}
