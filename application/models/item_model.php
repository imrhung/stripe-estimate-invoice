<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_model extends CI_Model
{
	function Item_model()
	{
		parent::__construct();
	}
	function getItem()
	{
		$this->load->database();
		$query = $this->db->get('item');
		$query = $this->db->query('SELECT * FROM item WHERE id='.$this->input->post('itemID'));
		return $query->result();
	}
	function addItemEstimate()
	{
		$this->load->database();
		$this->db->trans_start();
		
		$data = array(
					'quantity' => $this->input->post('itemQuantity'),
					'name' => $this->input->post('itemKind'),
					'description' => $this->input->post('itemDescription'),
					'price' => $this->input->post('itemPrice'),
					'estimate_id' => $this->input->post('itemEstimateId')
					);
		$str = $this->db->insert_string('item',$data);
		$this->db->query($str);
		
		$query = $this->db->query('SELECT * FROM item WHERE id='.$this->db->insert_id());
		
		$this->db->trans_complete();
		return $query->result();
	}
	function removeItem()
	{
		$this->load->database();
		$query = $this->db->query('DELETE FROM item WHERE id='.$this->input->post('itemID').';');
		return $this->db->affected_rows();
	}
}