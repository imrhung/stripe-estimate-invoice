<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_model extends CI_Model {

    function Payment_model() {
        parent::__construct();
    }

    function createClientEstimate() {

        $parameters = array($this->input->post('clientName'),
            $this->input->post('clientEmail'),
            $this->input->post('clientAddress'),
            $this->input->post('clientSummary'),
            $this->input->post('createdDate')
        );

        $sql = "CALL add_estimate(?,?,?,?,?)";
        $query = $this->db->query($sql, $parameters);
        return $query->result();
    }
    
    public function getEstimateId($estimateUid){
        $query = $this->db->get_where('estimate_uid', array('uid'=> $estimateUid));
        $result = $query->row_array();
        if ($result){
            return (int) $result['estimate_id'];
        } else {
            return false;
        }
    }
    
    public function getAmount($estimateId){
        $query = $this->db->query("SELECT SUM(price*quantity) as total_value from item WHERE estimate_id = $estimateId");
        $result = $query->row_array();
        if ($result){
            return (int) $result['total_value'];
        } else {
            return false;
        }
    }

}
