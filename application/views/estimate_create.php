<!--ESTIMATE REVIEW FORM-->
<div class="col-sm-8 col-md-8 col-lg-8">
	<table class="table table-curved" id="table-list">
		<tr>
			<th>
			<div class="row">
				<div class="col-sm-8 col-md-8 col-lg-8"></div>
				<div class="col-sm-4 col-md-4 col-lg-4"><p class="text-right">COST ESTIMATE</p></div>
			</div>
			</th>
		</tr>
		<tr>
			<td>
			<div class="row">
				<div class="col-sm-2 col-md-2 col-lg-2">
				<p class="text-muted">Estimate ID:</p>
				<p class="text-muted">Created Date:</p>
				<p class="text-muted">Issue Date:</p>
				<p class="text-muted">Summary:</p>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
				<p id="contractId"></p>
				<p id="contractCreatedDate"></p>
				<p id="contractIssueDate"></p>
				<p id="contractSummary"></p>
				</div>
				<div class="col-sm-7 col-md-7 col-lg-7">
					<div class="row">
						<div class="col-sm-2 col-md-2 col-lg-2"><p class="text-muted text-right">TO:</p></div>
						<div class="col-sm-10 col-md-10 col-lg-10">
							<p id="contractClientName"></p>
							<p id="contractClientAddress"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-2 col-lg-2"><p class="text-muted text-right">FROM:</p></div>
						<div class="col-sm-10 col-md-10 col-lg-10">
							<p>Align Lab</p>
							<p>alignlab.com</p>
							<p>info@alignlab.com</p>
							<p>USA: 615.852.5826</p>
							<p>VN: 090.282.031</p>
						</div>
					</div>
				</div>
			</div>
			<!--ITEM TABLE-->
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<table class="table table-thin-line" id="table-thin-line">
					<tr>
						<td>Qty</td>
						<td>Kind</td>
						<td>Description</td>
						<td>Price</td>
						<td>Amount</td>
					</tr>					
				</table>
				</div>
				
			</div>
			<!--CONTRACT FOOTER-->				
			<div class="row">
				<div class="col-sm-9 col-md-9 col-lg-9">
				<p class="text-right">Total:</p>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
				<p id="totalAmount"></p>
				</div>
			</div>
			</td>
		</tr>
	</table>
</div>
<!--MIDDLE SPACE-->
<div class="col-sm-1 col-md-1 col-lg-1">
</div>
<!--ITEM INPUT FORM-->
<div class="col-sm-3 col-md-3 col-lg-3 hidden" id="itemForm">
	<form role="form" id="newItemForm" action="index.php/estimate/addItem">
		<input type="hidden" id="itemEstimateId" name="itemEstimateId">
		<div class="form-group">
			<label><h3>ADD ITEM</h3></label><br>
			<label for="itemQuantity">Quantity</label>
			<input type="text" class="form-control" id="itemQuantity" placeholder="Enter quantity" name="itemQuantity">
		</div>
		<div class="form-group">
			<label for="itemKind">Kind</label>
			<input type="text" class="form-control" id="itemKind" placeholder="Enter item's kind" name="itemKind">
		</div>
		<div class="form-group">
			<label for="itemDescription">Description</label>
			<textarea rows="3"  class="form-control" id="itemDescription" placeholder="Enter description" name="itemDescription"/>
		</div>
		<div class="form-group">
			<label for="itemPrice">Price</label>
			<input type="text" class="form-control" id="itemPrice" placeholder="Enter price" name="itemPrice">
		</div>
		<button type="submit" class="btn btn-default btn-lg btn-block">Save</button>
	</form>
</div>
<!--ESTIMATE INPUT FORM-->
<div class="col-sm-3 col-md-3 col-lg-3" id="estimateForm">
	<form role="form" id="newClientForm" action="index.php/estimate/createEstimate">
		<label><h3>CREATE ESTIMATE</h3></label>
		<div class="form-group">			
			<label for="clientName">Client's Name</label>
			<input type="text" class="form-control" id="clientName" placeholder="Enter client's name" name="clientName">
		</div>
		<div class="form-group">
			<label for="clientEmail">Client's Email</label>
			<input type="email" class="form-control" id="clientEmail" placeholder="Enter client's email" name="clientEmail">
		</div>
		<div class="form-group">
			<label for="clientAddress">Estimate's Address</label>
			<input type="text" class="form-control" id="clientAddress" placeholder="Enter order address" name="clientAddress">
		</div>
		<div class="form-group">
			<label for="clientSummary">Summary</label>
			<textarea rows="3"  class="form-control" id="clientSummary" placeholder="Enter summary" name="clientSummary"/>
		</div>
		<div class="form-group">
		<label for="createdDate">Created Date</label>
		<div class="input-group date" id="datetimepicker">
		  <input type="text" class="form-control" placeholder="Please select a date" id="createdDate" name="createdDate">
		  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		</div>
		</div>
		<button type="submit" class="btn btn-default btn-lg btn-block">Save</button>
	</form>
</div>
<script type="text/javascript" src="assets/js/moment.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/screen-estimate-create.js"></script>