
<!-- LEFT COLUMN -->
<div class="col-sm-8 col-md-8 col-lg-8">
<div class="row">
<p>RECENT OPEN ESTIMATES</p>
<p>Below are estimates you have created in the past 30 days</p>
</div>
<div class="row">
	<table class="table table-curved table-hover" id="table-list">
		<tr>
			<td><!--a class="btn btn-default" onClick='removeAllItemInTable();'>Clear All</a--></td>
			<td><b>Open Estimates</b></td>
			<td><b>Date</b></td>
			<td><b>Amount</b></td>			
		</tr>
	</table>
</div>
<div class="row">
<p>RECENT CLOSE ESTIMATES</p>
<p>Below are estimates you have created in the past 30 days</p>
</div>
<div class="row">
	<table class="table table-curved table-hover" id="closed-estimates-list">
		<tr>
			<td><!--a class="btn btn-default" onClick='removeAllItemInTable();'>Clear All</a--></td>
			<td><b>Closed Estimates</b></td>
			<td><b>Date</b></td>
			<td><b>Amount</b></td>			
		</tr>
	</table>
</div>
</div>
<!-- MIDDLE COLUMN -->
<div class="col-sm-1 col-md-1 col-lg-1">
</div>
<!-- RIGHT COLUMN -->
<div class="col-sm-3 col-md-3 col-lg-3">
<form class="form-horizontal" role="form" id="newItemForm">			
	<div class="row">
	<p>ESTIMIATES AMOUNTS</p>	
	</div>
	<div class="row">
		<div class="col-sm-7 col-md-7 col-lg-7"><p>Open estimates</p></div>
		<div class="col-sm-5 col-md-5 col-lg-5"><p>$5,600.00 Not yet</p></div>
	</div>
	<div class="row">
		<div class="col-sm-7 col-md-7 col-lg-7"><p>Won estimates</p></div>
		<div class="col-sm-5 col-md-5 col-lg-5"><p>$5,600.00 TODO</p></div>
	</div>
	<div class="row">
		<a class="btn btn-default btn-lg" style="width:100%;" onclick="showEstimateCreatedScreen();">CREATE ESTIMATES</a>
		<div class="col-sm-12 col-md-12 col-lg-12"></div>				
	</div>
	<!-- <div class="row">
		<div class="form-group">
			<div class="col-sm-7 col-md-7 col-lg-7">			  
				<input type="text" class="form-control" name="itemName" placeholder="Enter Item Name">
			</div>
			<div class="col-sm-1 col-md-1 col-lg-1">				
				<button type="submit" class="btn btn-default">Add Item</button>
			</div>
		</div>
	</div> -->
</form>
</div>
<script type="text/javascript" src="assets/js/screen-estimate-overview.js"></script>