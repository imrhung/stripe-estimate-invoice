<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="vhson13@gmail.com">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Narrow Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->   
	
	<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css"/>
	<link rel="stylesheet" href="assets/css/bootstrapValidator.min.css"/>
	<link rel="stylesheet" href="assets/css/custom.css" type="text/css"/>

    <!-- Custom styles for this template -->    
	<!--link rel="stylesheet" href="assets/css/jumbotron-narrow.css" type="text/css"/-->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/bootstrapValidator.min.js"></script>
	

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script>
	var iSelectedEstimateId = 0;
	var iTotalAmount = 0;
	/*
	var iItemInitId = 0;
	function addItem()
	{
		iItemInitId++;
		var jsonObj = {"id":null};
		jsonObj.id = 'item_'+ iItemInitId;
		$('#item-template').tmpl(jsonObj).appendTo('#item-list');
	}
	function removeDummyItem(item)
	{
		$('#'+item.id).remove();
	}
	function removeItem(item)
	{
		console.log("remove = "+item);
		$('#row_'+item).remove();
		$.ajax({
			type: "POST",
			url: "index.php/welcome/removeItem",
			data: {itemID:item},
			dataType: "json",
			success: function (result) {
				// alert('number affected rows = '+result);
			}
		});
	}
	function removeAllItemInTable()
	{
		$('#table-list').find("tr:gt(0)").remove();
	}
	*/
	function showEstimateOverviewScreen()
	{
		if($('#content-view').html())
		{
			$('#content-view').fadeOut('fast',function(){
				$('#content-view').empty();			
			});
		}
		$.ajax({
			type: "POST",
			url: "index.php/welcome/estimateOverview",
			data: {},
			dataType: "html",
			success: function (result) {					
				$('#content-view').fadeIn('fast',function(){
					$('#content-view').append(result);
				});
			}
		});
	}
	function showEstimateCreatedScreen()
	{
		$('#content-view').fadeOut('fast',function(){
			$('#content-view').empty();			
		});
		$.ajax({
			type: "POST",
			url: "index.php/welcome/estimateCreateView",
			data: {},
			dataType: "html",
			success: function (result) {					
				$('#content-view').fadeIn('fast',function(){
					$('#content-view').append(result);
				});
			}
		});
	}
	function showEstimateEditScreen()
	{
		$('#content-view').fadeOut('fast',function(){
			$('#content-view').empty();			
		});
		$.ajax({
			type: "POST",
			url: "index.php/welcome/estimateEditView",
			data: {},
			dataType: "html",
			success: function (result) {					
				$('#content-view').fadeIn('fast',function(){
					$('#content-view').append(result);
				});
			}
		});
	}
	function showScreen(screen_url)
	{
		$('#content-view').fadeOut('fast',function(){
			$('#content-view').empty();			
		});
		$.ajax({
			type: "POST",
			url: screen_url,
			data: {},
			dataType: "html",
			success: function (result) {					
				$('#content-view').fadeIn('fast',function(){
					$('#content-view').append(result);
				});
			}
		});
	}
	function removeItem(id,price)
	{
		iTotalAmount -= price;
		$('#totalAmount').html('$'+iTotalAmount);
		$.ajax({
			type: "POST",
			url: "index.php/estimate/removeItem",
			data: {itemID:id},
			dataType: "json",
			success: function (result) {
				$('#row_'+id).remove();
				
			}
		});
	}
	
	$(document).ready(function(){
		showEstimateOverviewScreen();
		
		
	});
	</script>
	
  </head>

  <body>

	<!-- NAVIGATION BAR -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="#" class="navbar-brand">INVOICES</a>
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse" id="navbar-main">
					<ul class="nav navbar-nav navbar-right">
						<li>
						<!--a href="#" onclick="showEstimateOverviewScreen();">ESTIMATE</a-->
						<a href="#" onclick="showScreen('index.php/welcome/estimateOverview');">ESTIMATE</a>
						</li>
						<li>
						<a href="#" onclick="showScreen('index.php/welcome/paymentView');">INVOICES</a>
						</li>
						<li>
						<a href="user_guide/">BUTTON</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	<!-- CONTENT SECTION -->
    <div class="container" style="padding-top:80px;padding-bottom:80px;" id="content-view">
    </div>
	<!-- FOOTER SECTION -->

	<div id="footer" class="hidden-xs container">
		<nav class="navbar navbar-default navbar-fixed-bottom text-center" style="padding-top:10px;">
			<p class="text-muted">Copyright © 2014 Align Lab. All rights reserved.</p>
		</nav>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
	<script id="item-template" type="text/x-jquery-tmpl">
	<div class="row ${id}" id="${id}">
		<div class="col-sm-10 col-md-10 col-lg-10"><div class="well well-sm">${id}</div></div>
		<div class="col-sm-2 col-md-2 col-lg-2 btn-group">
			<a class="btn btn-default" onClick='addItem();'>+</a>
			<a class="btn btn-default" onClick='removeDummyItem(${id});'>-</a>
		</div>
	</div>
	</script>
	<script id="itemdata-template" type="text/x-jquery-tmpl">
	<tr id="row_${id}">
		<td>
			<input type="checkbox" value="${id}"/>
		</td>
		<td style="width:50%;">
			<div class="row" onmouseover="mouseOverOnOpenEstimateTable(this);" onmouseout="mouseOutOnOpenEstimateTable(this);" id="item_${id}">
				<div class="col-sm-9 col-md-9 col-lg-9">
				${client_name}
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3 hidden" id="toolBar${id}" style="padding-left:15px;"><a onclick="viewEstimate(${estimate_id});"><span class="glyphicon glyphicon-pencil"></span></a> <a onClick="removeItem(${id});"><span class="glyphicon glyphicon-trash"></span></a></div>				
			</div>
		</td>
		<td>${created_date}</td>
		<td>$${total_value}</td>
	</tr>
	</script>
	<script id="estimateItem-template" type="text/x-jquery-tmpl">
	<tr id="row_${id}">
		<td>${quantity}</td>		
		<td style="width:30%;">			
			<div class="row" onmouseover="mouseOverOnOpenEstimateTable(this);" onmouseout="mouseOutOnOpenEstimateTable(this);" id="item_${id}">
				<div class="col-sm-9 col-md-9 col-lg-9">
				${name}
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3 hidden" id="toolBar${id}" style="padding-left:15px;"><a onClick="removeItem(${id},${price});"><span class="glyphicon glyphicon-trash"></span></a></div>			
			</div>
		</td>		
		<td style="width:40%;">${description}</td>		
		<td>${price}</td>		
		<td>${value}</td>		
	</tr>
	</script>
  </body>
</html>
