<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Buy This Thing</title>
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</head>
<body><?php 

// Set the Stripe key:
// Uses STRIPE_PUBLIC_KEY from the config file.
echo '<script type="text/javascript">Stripe.setPublishableKey("' . STRIPE_PUBLIC_KEY . '");</script>';

?>

    <!--ESTIMATE REVIEW FORM-->
<div class="col-sm-8 col-md-8 col-lg-8">
    <table class="table table-curved" id="table-list">
        <tr>
            <th>
        <div class="row">
            <div class="col-sm-8 col-md-8 col-lg-8"></div>
            <div class="col-sm-4 col-md-4 col-lg-4"><p class="text-right">COST ESTIMATE</p></div>
        </div>
        </th>
        </tr>
        <tr>
            <td>
                <div class="row">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <p class="text-muted">Estimate ID:</p>
                        <p class="text-muted">Created Date:</p>
                        <p class="text-muted">Issue Date:</p>
                        <p class="text-muted">Summary:</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p id="contractId"></p>
                        <p id="contractCreatedDate"></p>
                        <p id="contractIssueDate"></p>
                        <p id="contractSummary"></p>
                    </div>
                    <div class="col-sm-7 col-md-7 col-lg-7">
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2"><p class="text-muted text-right">TO:</p></div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <p id="contractClientName"></p>
                                <p id="contractClientAddress"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2"><p class="text-muted text-right">FROM:</p></div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <p>Align Lab</p>
                                <p>alignlab.com</p>
                                <p>info@alignlab.com</p>
                                <p>USA: 615.852.5826</p>
                                <p>VN: 090.282.031</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--ITEM TABLE-->
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-thin-line" id="table-thin-line">
                            <tr>
                                <td>Qty</td>
                                <td>Kind</td>
                                <td>Description</td>
                                <td>Price</td>
                                <td>Amount</td>
                            </tr>					
                        </table>
                    </div>

                </div>
                <!--CONTRACT FOOTER-->				
                <div class="row">
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <p class="text-right">Total:</p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p id="totalAmount">$1000000</p>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
    
    
    
    
    
    
    
	<h1>Buy This Thing</h1>

	<form action="buy.php" method="POST" id="payment-form">

		<?php // Show PHP errors, if they exist:
		if (isset($errors) && !empty($errors) && is_array($errors)) {
			echo '<div class="alert alert-error"><h4>Error!</h4>The following error(s) occurred:<ul>';
			foreach ($errors as $e) {
				echo "<li>$e</li>";
			}
			echo '</ul></div>';	
		}?>

		<div id="payment-errors"></div>

		<span class="help-block">You can pay using: Mastercard, Visa, American Express, JCB, Discover, and Diners Club.</span>

		<div class="alert alert-info"><h4>JavaScript Required!</h4>For security purposes, JavaScript is required in order to complete an order.</div>

		<label>Card Number</label>
		<input type="text" size="20" autocomplete="off" class="card-number input-medium">
		<span class="help-block">Enter the number without spaces or hyphens.</span>
		<label>CVC</label>
		<input type="text" size="4" autocomplete="off" class="card-cvc input-mini">
		<label>Expiration (MM/YYYY)</label>
		<input type="text" size="2" class="card-expiry-month input-mini">
		<span> / </span>
		<input type="text" size="4" class="card-expiry-year input-mini">

		<button type="submit" class="btn" id="submitBtn">Submit Payment</button>

	</form>

	<script src="js/buy.js"></script>
        <!--<script type="text/javascript" src="assets/js/moment.js"></script>-->
<script type="text/javascript" src="assets/js/pay.js"></script>

</body>
</html>