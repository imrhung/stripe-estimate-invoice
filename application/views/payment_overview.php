
<!-- LEFT COLUMN -->
<div class="col-sm-8 col-md-8 col-lg-8">
    
    <!--Custom button here: https://stripe.com/docs/checkout/v2-->
    <form id="payment-form" method="POST" action="index.php/payment/chargeInvoice">
        <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"
            data-amount="2000"
            data-name="Demo pay"
            data-description="2 widgets ($20.00)"
            data-image="/128x128.png">
        </script>
    </form>

    <form role="form" id="payment-form">

        <input type="hidden" id="itemEstimateId" name="itemEstimateId">			
        <label><h3>CARD INFORMATION</h3></label><br>
        <span class="payment-errors"></span>
        <div class="form-group">
            <label for="itemQuantity">Card Number</label>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="cardNumber" placeholder="Enter Card Number" name="cardNumber" data-stripe="number">
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <input type="text" class="form-control" id="cardCVC" placeholder="CVC" name="cardCVC" data-stripe="cvc">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="itemKind">Expiration</label>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="cardMonth" placeholder="MM" name="cardMonth" data-stripe="exp-month">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <input type="text" class="form-control" id="cardYear" placeholder="YY" name="cardYear" data-stripe="exp-year">
                </div>
            </div>
        </div>		
        <button type="submit" class="btn btn-default btn-lg btn-block">Save</button>
        <!--a class="btn btn-default btn-lg btn-block" onclick="showFormAddClient();">Back</a-->
    </form>
</div>
<!-- MIDDLE COLUMN -->
<div class="col-sm-1 col-md-1 col-lg-1">
</div>
<!-- RIGHT COLUMN -->
<div class="col-sm-3 col-md-3 col-lg-3">

</div>
<script type="text/javascript">
    // This identifies your website in the createToken call below
    Stripe.setPublishableKey('pk_test_ilPl1t7wv0l2LvYboZpTurxL');
</script>
<script type="text/javascript" src="assets/js/screen-payment-overview.js"></script>