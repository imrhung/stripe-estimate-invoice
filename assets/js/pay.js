function getEstimateItems(estimateId)
{
    $.ajax({
        type: "POST",
        url: "index.php/estimate/getEstimateItemList",
        data: {estimate_id: estimateId},
        dataType: "json",
        success: function (result) {
            /* console.log(result); */
            iTotalAmount = 0;
            $.each(result, function (i, item) {
                item.value = item.price * item.quantity;
                iTotalAmount += item.value;
                $('#estimateItem-template').tmpl(item).appendTo('#table-thin-line');
            });
            $('#totalAmount').html('$' + iTotalAmount);
        }
    });
}
function getEstimateInfo(estimateId)
{
    $.ajax({
        type: "POST",
        url: "index.php/estimate/getEstimateInfo",
        data: {estimate_id: estimateId},
        dataType: "json",
        success: function (result) {
            /* console.log(result); */
            data = result[0];
            $('#contractId').html(data.id);
            $('#contractCreatedDate').html(data.created_date);
            $('#contractIssueDate').html(data.issue_date);
            $('#contractSummary').html(data.sumary);
            $('#contractClientName').html(data.client_name);
            $('#contractClientAddress').html(data.address);
            $('#clientId').attr('value', data.client_id);
            /* $('#clientName').attr('value',data.client_name);
             $('#clientEmail').attr('value',data.client_email); */
            $('#clientAddress').attr('value', data.address);
            $('#clientSummary').html(data.sumary);
            $('#itemEstimateId').attr('value', data.id);
            getEstimateItems();

        }
    });
}

$(function(){
    getEstimateInfo()
});