$(function () {
	$('#datetimepicker').datetimepicker();
	
		$('#newItemForm').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			submitHandler: function(validator,form,submitButton){
				$.post(form.attr('action'), form.serialize(), function(result) {
					// ... process the result ...
					
					$.each(result,function(i,item){
						item.value = item.price * item.quantity;
						iTotalAmount += item.value;
						$('#estimateItem-template').tmpl(item).appendTo('#table-thin-line');
					});
					$('#totalAmount').html('$'+iTotalAmount);
					validator.resetForm();
				}, 'json');
			},
			fields: {
				itemQuantity: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The quantity is required and cannot be empty'
						},						
						regexp: {
							regexp: /^[0-9]+$/,
							message: 'The quantity can only consist of number'
						}
					}
				},
				itemKind: {
					validators: {
						notEmpty: {
							message: 'The kind is required and cannot be empty'
						},
					}
				},
				itemPrice: {
					validators: {
						notEmpty: {
							message: 'The price is required and cannot be empty'
						},
						regexp: {
							regexp: /^[0-9.,]+$/,
							message: 'The quantity can only consist of number, comma and dot'
						}
					}
				}
			}
		});
		$('#newClientForm').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			submitHandler: function(validator,form,submitButton){
				$.post(form.attr('action'), form.serialize(), function(result) {
					var data = result[0];
					if(data.result == 1)
					{
						$('#contractId').html(data.estimate_id);
						$('#contractCreatedDate').html(data.created_date);
						$('#contractIssueDate').html(data.issue_date);
						$('#contractSummary').html(data.estimate_id);
						$('#contractClientName').html(data.client_name);
						$('#contractSummary').html(data.sumary);
						$('#contractClientAddress').html(data.address);
						$('#itemEstimateId').attr('value',data.estimate_id);
						showFormAddItem();
						validator.resetForm();
					}
				}, 'json');
			},
			fields: {
				clientName: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The username is required and cannot be empty'
						}
					}
				},
				clientEmail: {
					validators: {
						notEmpty: {
							message: 'The email is required and cannot be empty'
						},
						emailAddress: {
							message: 'The input is not a valid email address'
						}
					}
				},
				clientAddress: {
					message: 'The address is not valid',
					validators: {
						notEmpty: {
							message: 'The address is required and cannot be empty'
						}
					}
				}
			}
		});
	
	
/* 	$('#newClientForm').submit(function(event){
		var bootstrapValidator = $('#newClientForm').data('bootstrapValidator');
		if(bootstrapValidator.isValid())
		addNewClient();
		return false;
	}); */
	/* $('#newItemForm').submit(function(event){
		var bootstrapValidator = $('#newItemForm').data('bootstrapValidator');
		if(bootstrapValidator.isValid())
		addNewItem();
		return false;
	}); */
	
});

/* function addNewItem()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/addItem",
		data:  $("#newItemForm").serialize(),
		dataType: "json",
		success: function (result) {
			
			$.each(result,function(i,item){
				item.value = item.price * item.quantity;
				iTotalAmount += item.value;
				$('#estimateItem-template').tmpl(item).appendTo('#table-thin-line');
			});
			$('#totalAmount').html('$'+iTotalAmount);
		}
	});
}
function addNewClient()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/createEstimate",
		data:  $("#newClientForm").serialize(),
		dataType: "json",
		success: function (result) {
			console.log(result[0]);
			var data = result[0];
			if(data.result == 1)
			{
				$('#contractId').html(data.estimate_id);
				$('#contractCreatedDate').html(data.created_date);
				$('#contractIssueDate').html(data.issue_date);
				$('#contractSummary').html(data.estimate_id);
				$('#contractClientName').html(data.client_name);
				$('#contractSummary').html(data.sumary);
				$('#contractClientAddress').html(data.address);
				$('#itemEstimateId').attr('value',data.estimate_id);
				showFormAddItem();
			}
		}
	});
} */
function showFormAddItem()
{
	$('#itemForm').removeClass('hidden');
	$('#estimateForm').addClass('hidden');
}
/* function removeItem(int id,int price)
{
	iTotalAmount -= price;
	$('#totalAmount').html('$'+iTotalAmount);
	$.ajax({
		type: "POST",
		url: "index.php/estimate/removeItem",
		data: {itemID:id},
		dataType: "json",
		success: function (result) {
			$('#row_'+id).remove();
			
		}
	});
} */