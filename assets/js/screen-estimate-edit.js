$(function () {
	$('#datetimepicker').datetimepicker();
	
	
	$('#newItemForm').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			submitHandler: null,
			fields: {
				itemQuantity: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The quantity is required and cannot be empty'
						},						
						regexp: {
							regexp: /^[0-9]+$/,
							message: 'The quantity can only consist of number'
						}
					}
				},
				itemKind: {
					validators: {
						notEmpty: {
							message: 'The kind is required and cannot be empty'
						},
					}
				},
				itemPrice: {
					validators: {
						notEmpty: {
							message: 'The price is required and cannot be empty'
						},
						regexp: {
							regexp: /^[0-9.,]+$/,
							message: 'The quantity can only consist of number, comma and dot'
						}
					}
				}
			}
		});
		$('#newClientForm').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			submitHandler: null,
			fields: {
				clientName: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The username is required and cannot be empty'
						},
						stringLength: {
							min: 6,
							max: 30,
							message: 'The username must be more than 6 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[a-zA-Z0-9_]+$/,
							message: 'The username can only consist of alphabetical, number and underscore'
						}
					}
				},
				clientEmail: {
					validators: {
						notEmpty: {
							message: 'The email is required and cannot be empty'
						},
						emailAddress: {
							message: 'The input is not a valid email address'
						}
					}
				},
				clientAddress: {
					message: 'The address is not valid',
					validators: {
						notEmpty: {
							message: 'The address is required and cannot be empty'
						}
					}
				},
				createdDate: {
					message: 'The date is not valid',
					validators: {
						notEmpty: {
							message: 'The date is required and cannot be empty'
						}
					}
				}
			}
		});
	
	
	
	
	$('#newClientForm').submit(function(event){
		updateEstimate();
		return false;
	});
	$('#newItemForm').submit(function(event){
		addNewItem();
		return false;
	});
	getEstimateInfo(iSelectedEstimateId);
	$('#estimateId').attr('value',iSelectedEstimateId);
});
function updateEstimate()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/updateEstimateInfo",
		data: $("#newClientForm").serialize(),
		dataType: "json",
		success: function (result) {
			console.log(result);
			data = result[0];
			$('#contractSummary').html(data.sumary);			
			$('#contractClientAddress').html(data.address);
			$('#contractIssueDate').html(data.issue_date);			
		}
	});
}
function getEstimateItems()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/getEstimateItemList",
		data: {estimate_id:iSelectedEstimateId},
		dataType: "json",
		success: function (result) {
			/* console.log(result); */
			iTotalAmount = 0;
			$.each(result,function(i,item){				
				item.value = item.price * item.quantity;
				iTotalAmount += item.value;
				$('#estimateItem-template').tmpl(item).appendTo('#table-thin-line');
			});
			$('#totalAmount').html('$'+iTotalAmount);
		}
	});
}
function getEstimateInfo(estimateId)
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/getEstimateInfo",
		data: {estimate_id:estimateId},
		dataType: "json",
		success: function (result) {
			/* console.log(result); */
			data = result[0];
			$('#contractId').html(data.id);
			$('#contractCreatedDate').html(data.created_date);
			$('#contractIssueDate').html(data.issue_date);
			$('#contractSummary').html(data.sumary);
			$('#contractClientName').html(data.client_name);
			$('#contractClientAddress').html(data.address);
			$('#clientId').attr('value',data.client_id);
			/* $('#clientName').attr('value',data.client_name);
			$('#clientEmail').attr('value',data.client_email); */
			$('#clientAddress').attr('value',data.address);
			$('#clientSummary').html(data.sumary);
			$('#itemEstimateId').attr('value',data.id);
			getEstimateItems();
			
		}
	});
}
function mouseOverOnOpenEstimateTable(sender)
{
	var jObj = $(sender);
	var id = sender.id.slice(5);
	var row = $(jObj).find('#toolBar'+id).removeClass('hidden');
	//console.log("==> rowID = "+$(row).attr('id'));
}
function mouseOutOnOpenEstimateTable(sender)
{
	var jObj = $(sender);
	var id = sender.id.slice(5);
	var row = $(jObj).find('#toolBar'+id).addClass('hidden');
	//console.log("==> rowID = "+$(row).attr('id'));
}
function showFormAddItem()
{
	$('#itemForm').removeClass('hidden');
	$('#estimateForm').addClass('hidden');
}
function showFormAddClient()
{
	$('#estimateForm').removeClass('hidden');
	$('#itemForm').addClass('hidden');
}
/* function removeItem(int id,int price)
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/removeItem",
		data: {itemID:id},
		dataType: "json",
		success: function (result) {
			$('#row_'+id).remove();
		}
	});
} */
function editItem(id)
{
	showFormAddItem();
	$.ajax({
		type: "POST",
		url: "index.php/estimate/getItem",
		data: {itemID:id},
		dataType: "json",
		success: function (result) {
			console.log(result);
			data = result[0];
			$('#itemQuantity').attr('value',data.quantity);
			$('#itemKind').attr('value',data.name);
			$('#itemDescription').html(data.description);
			$('#itemPrice').attr('value',data.price);
			$('#itemEstimateId').attr('value',data.id);
		}
	});
}
function addNewItem()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/addItem",
		data:  $("#newItemForm").serialize(),
		dataType: "json",
		success: function (result) {
			console.log(result);
			$.each(result,function(i,item){
				item.value = item.price * item.quantity;
				iTotalAmount+=item.value;
				$('#estimateItem-template').tmpl(item).appendTo('#table-thin-line');
			});
			$('#totalAmount').html('$'+iTotalAmount);
		}
	});
}