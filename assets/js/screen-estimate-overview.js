function mouseOverOnOpenEstimateTable(sender)
{
	var jObj = $(sender);
	var id = sender.id.slice(5);
	var row = $(jObj).find('#toolBar'+id).removeClass('hidden');
	//console.log("==> rowID = "+$(row).attr('id'));
}
function mouseOutOnOpenEstimateTable(sender)
{
	var jObj = $(sender);
	var id = sender.id.slice(5);
	var row = $(jObj).find('#toolBar'+id).addClass('hidden');
	//console.log("==> rowID = "+$(row).attr('id'));
}
function getOpenEstimateData()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/recentOpenEstimates",
		data: {},
		dataType: "json",
		success: function (result) {
                    console.log(result);
			$.each(result,function(i,item){
				$('#itemdata-template').tmpl(item).appendTo('#table-list');
			});
		}
	});
}
function getClosedEstimateData()
{
	$.ajax({
		type: "POST",
		url: "index.php/estimate/recentClosedEstimates",
		data: {},
		dataType: "json",
		success: function (result) {
			$.each(result,function(i,item){
				$('#itemdata-template').tmpl(item).appendTo('#closed-estimates-list');
			});
		}
	});
}

function viewEstimate(estimateId)
{
	iSelectedEstimateId = estimateId;
	showEstimateEditScreen();
	/* $.ajax({
		type: "POST",
		url: "index.php/welcome/removeItem",
		data: {itemID:item},
		dataType: "json",
		success: function (result) {
		}
	}); */
}
$(function(){
	getOpenEstimateData();
	getClosedEstimateData();
});