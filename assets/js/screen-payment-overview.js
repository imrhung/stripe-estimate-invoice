$(function(){
	$('#payment-form').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			submitHandler: null,
			fields: {
				itemQuantity: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The quantity is required and cannot be empty'
						},						
						regexp: {
							regexp: /^[0-9]+$/,
							message: 'The quantity can only consist of number'
						}
					}
				},
				itemKind: {
					validators: {
						notEmpty: {
							message: 'The kind is required and cannot be empty'
						},
					}
				},
				itemPrice: {
					validators: {
						notEmpty: {
							message: 'The price is required and cannot be empty'
						},
						regexp: {
							regexp: /^[0-9.,]+$/,
							message: 'The quantity can only consist of number, comma and dot'
						}
					}
				}
			}
		});
	$('#payment-form').submit(function(event){
	
		var $form = $(this);

		// Disable the submit button to prevent repeated clicks
		$form.find('button').prop('disabled', true);

		Stripe.card.createToken($form, function(status,response){
				// console.log(response);
				var $form = $('#payment-form');

				if (response.error) {
					// Show the errors on the form
					$form.find('.payment-errors').html('<div class="btn btn-danger">'+response.error.message+'</div>');
					$form.find('button').prop('disabled', false);
				} else {
					// response contains id and card, which contains additional card details
					var token = response.id;
					// Insert the token into the form so it gets submitted to the server
					$form.append($('<input type="hidden" name="stripeToken" />').val(token));
					// and submit
					//$form.get(0).setAttribute('action','index.php/payment/chargeInvoice');
					//$form.get(0).submit();
					
					$.ajax({
						type: "POST",
						url: 'index.php/payment/chargeInvoice',
						data: response,
						dataType: "html",
						success: function (result) {					
							$('#content-view').fadeIn('fast',function(){
								$('#content-view').append(result);
							});
						}
					});
				}
		});
		return false;
	});
});